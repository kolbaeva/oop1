#pragma once
#include "PointCloud.h"

class PassThroughFilter
{
    private:
        double upperLimitX;
        double lowerLimitX;

        double upperLimitY;
        double lowerLimitY;

        double upperLimitZ;
        double lowerLimitZ;

    public:
        ///<summary>Constructor for PassThroughFilter Class </summary>
        PassThroughFilter(double _uX=0, double _lX=0, double _uY=0, double _lY=0, double _uZ=0, double _lZ=0)
        :upperLimitX(_uX), lowerLimitX(_lX), upperLimitY(_uY), lowerLimitY(_lY), upperLimitZ(_uZ), lowerLimitZ(_lZ) { };
        
        ///<summary>Filter function </summary>
	    ///<param name ="pc">Value to filter</param>
        void filter(PointCloud &pc); 
        
        ///<summary>Get function for upper limit x </summary>
        double getUpperLimitX();
        ///<summary>Set upper for limit x </summary>
	    ///<param name ="uX">Value to set upper limit x</param>
        void setUpperLimitX(double);
        
        ///<summary>Get function for lower limit x </summary>
        double getLowerLimitX();
        ///<summary>Set lower for limit x </summary>
	    ///<param name ="lX">Value to set lower limit x</param>
        void setLowerLimitX(double);

        ///<summary>Get function for upper limit y </summary>
        double getUpperLimitY();
        ///<summary>Set upper for limit y </summary>
	    ///<param name ="uY">Value to set upper limit y</param>
        void setUpperLimitY(double);
        
        ///<summary>Get function for lower limit y </summary>
        double getLowerLimitY();
        ///<summary>Set lower for limit y </summary>
	    ///<param name ="lY">Value to set lower limit y</param>
        void setLowerLimitY(double);

        ///<summary>Get function for upper limit z </summary>
        double getUpperLimitZ();
        ///<summary>Set upper for limit z </summary>
	    ///<param name ="uZ">Value to set upper limit z</param>
        void setUpperLimitZ(double);
        
        ///<summary>Get function for lower limit z </summary>
        double getLowerLimitZ();
        ///<summary>Set lower for limit z </summary>
	    ///<param name ="lZ">Value to set lower limit z</param>
        void setLowerLimitZ(double);

};

