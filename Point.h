#pragma once
class Point
{
private:
	double x, y, z;

public:
	///<summary>Constructor of Point object</summary>
	Point(double _x=0, double _y=0, double _z=0) :x(_x), y(_y), z(_z) { };

	///<summary>Set function for x </summary>
	///<param name ="_x">Value to set x</param>
	void setX(double);

	///<summary>Set function for x,y,z </summary>
	///<param name ="_y">Value to set y</param>
	void setY(double);

	///<summary>Set function for z </summary>
	///<param name ="_z">Value to set z</param>
	void setZ(double);

	///<summary>Return to value of x</summary>
	double getX()const;

	///<summary>Return to value of y</summary>
	double getY()const;

	///<summary>Return to value of z</summary>
	double getZ()const;

	///<summary>Adding point</summary>
	Point operator +(const Point& p);

	///<summary>Equal operator</summary>
	///<param name="p">Second point to compare the point</param>
	bool operator ==(const Point& p) const;

	///<summary>Calculate the distance between two points</summary>
	///<return>Return to distance value</return>
	double distance(const Point& p) const;
};

