#pragma once
#include<string>
#include <fstream>
#include <sstream>
#include "PointCloud.h"
using namespace std;

class PointCloudRecorder
{
private:
	string filename;

public:
	///<summary>Constructor</summary>
	PointCloudRecorder(string _filename) :filename(_filename) { };

	///<summary>Set filename</summary>
	void setFilename(string);

	///<summary>Get Filename</summary>
	string getFilename();

	///<summary>Save to file</summary>
	bool save(PointCloud&);

};

