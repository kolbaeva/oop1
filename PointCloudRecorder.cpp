#include "PointCloudRecorder.h"


//set filename
void PointCloudRecorder::setFilename(string filename) {
	this->filename = filename;
}

string PointCloudRecorder::getFilename()
{
	return filename;
}

bool PointCloudRecorder::save(PointCloud& pc)
{
	ofstream file;
	file.open(filename);
	char ch = ' ';
	for (int i = 0; i < pc.getPointnumber(); i++) {
		file << pc.getPoint(i).getX() << ch << pc.getPoint(i).getY() << ch << pc.getPoint(i).getZ()<<endl;

	}
	file.close();
	return true;
}
