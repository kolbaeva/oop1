#include "Transform.h"

Transform::Transform(double a[], double t[], double tm[4][4]) {
	for (int i = 0; i < 3; i++) {
		angles[i] = a[i];
		trans[i] = t[i];
	}
	for (int j = 0; j < 4; j++) {
		for (int k = 0; k < 4; k++) {
			transMatrix[j][k] = tm[j][k];
		}
	}
}

//set rotation
void Transform::SetRotation(double a[]) {
	for (int i = 0; i < 3; i++) {
		angles[i] = a[i];
	}
}

//set translation
void Transform::SetTranslation(double t[]) {
	for (int i = 0; i < 3; i++) {
		trans[i] = t[i];
	}
}

//dotransform
Point Transform::doTransform(Point p) {
	//a lot of code gonna came here
	return p;
}

//dotransformforpc
PointCloud Transform::doTransform(PointCloud pc) {
	//a lot of code gonna came here
	return pc;
}