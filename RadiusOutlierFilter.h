#pragma once
#include "PointCloud.h"

class RadiusOutlierFilter
{
private:
	double radius;

public:
	RadiusOutlierFilter(double _radius) :radius(_radius) {};

	///<summary>Set radius</summary>
	void setRadius(double);

	///<summary>Get radius</summary>
	double getRadius();

	///<summary>Filter</summary>
	void filter(PointCloud&);
};

