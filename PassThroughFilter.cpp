#include "PassThroughFilter.h"
        //get upper limit x
        double PassThroughFilter::getUpperLimitX()
        {
            return this->upperLimitX;
        }

        //set upper limit x
        void PassThroughFilter::setUpperLimitX(double upperLimitX)
        {
            this->upperLimitX = upperLimitX;
        }

        //get lower limit x
        double PassThroughFilter::getLowerLimitX()
        {
            return this->lowerLimitX;
        }
        
        //set lower limit x
        void PassThroughFilter::setLowerLimitX(double lowerLimitX)
        {
            this->lowerLimitX = lowerLimitX;
        } 
        
        //get upper limit y
        double PassThroughFilter::getUpperLimitY()
        {
            return this->upperLimitY;
        }
        
        //set upper limit y
        void PassThroughFilter::setUpperLimitY(double upperLimitY)
        {
            this->upperLimitY = upperLimitY;
        }
        
        //get lower limit y
        double PassThroughFilter::getLowerLimitY()
        {
            return this->lowerLimitY;
        }

        //set lower limit y
        void PassThroughFilter::setLowerLimitY(double lowerLimitY)
        {
            this->lowerLimitY = lowerLimitY;
        }
        
        //get upper limit z
        double PassThroughFilter::getUpperLimitZ()
        {
            return this->upperLimitZ;
        }
        
        //set upper limit z
        void PassThroughFilter::setUpperLimitZ(double upperLimitZ)
        {
            this->upperLimitZ = upperLimitZ;
        }

        //get lower limit z
        double PassThroughFilter::getLowerLimitZ()
        {
            return this->lowerLimitZ;
        }
        
        //set lower limit z
        void PassThroughFilter::setLowerLimitZ(double lowerLimitZ)
        {
            this->lowerLimitZ = lowerLimitZ;
        }
        
        //filter function
        void PassThroughFilter::filter(PointCloud &pc)
        {
            int i=0;
            double x, y, z, lX, uX, lY, uY, lZ, uZ;
            int pn = pc.getPointnumber();

            lX = this->getLowerLimitX();
            uX = this->getUpperLimitX();

            lY = this->getLowerLimitY();
            uY = this->getUpperLimitY();

            lZ = this->getLowerLimitZ();
            uZ = this->getUpperLimitZ();

            for(i; i<pn; i++){
               x = pc.points[i].getX();
               y = pc.points[i].getY();
               z = pc.points[i].getZ();

                if(x < lX || x > uX || y < lY || y > uY  || z < lZ || z > uZ ){
                   pc.deletePoint(i);
			       pn--;
                   i--;
                }
            }
        }
