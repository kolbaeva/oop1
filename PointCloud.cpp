#include "PointCloud.h"

//set
void PointCloud::setPoint(int i, const Point& p) {
	points[i] = p;
}
//set point number

//get
Point PointCloud::getPoint(int i) {
	return points[i];
}

int PointCloud::getPointnumber()
{
	return pointNumber;
}


//assign
void PointCloud::operator=(const PointCloud& pc) {
	this->points = pc.points;
	this->pointNumber = pc.pointNumber;
}

//plus op.
PointCloud PointCloud::operator+(const PointCloud& pc) {
	PointCloud tmp(this->pointNumber + pc.pointNumber);
	int i = 0;
	while(i < pointNumber) {
		tmp.points[i] = points[i];
		i++;
	}
	while (i < pointNumber + pc.pointNumber) {
		tmp.points[i] = pc.points[i];
		i++;
	}

	return tmp;
}

void PointCloud::setPointnumber(int pn)
{
	pointNumber = pn;
}

void PointCloud::deletePoint(int del)
{
	for (int k = del; k < (pointNumber - 1); k++)
	{
		this->setPoint(k, this->getPoint(k + 1));
	}
	--pointNumber;

}
