#include <iostream>
#include "DepthCamera.h"
#include <string>
#include "Transform.h"
#include "PointCloudRecorder.h"
#include "RadiusOutlierFilter.h"
#include "PassThroughFilter.h"
using namespace std;

int main() {
	PointCloud p1(886);
	PointCloud p2(884);
	DepthCamera dc1("camera1.txt");
	DepthCamera dc2("camera2.txt");
	PointCloudRecorder pcr1("example.txt");
	RadiusOutlierFilter rf1(25);
	PassThroughFilter ptf1(400, 0, 400, 0, 45, -45);
	p1 = dc1.capture(886);
	p2 = dc2.capture(884);
	ptf1.filter(p1);
	pcr1.save(p1);








	return 0;
}