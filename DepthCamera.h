#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include "PointCloud.h"
#include <iostream>
using namespace std;

class DepthCamera
{
private:
	string filename;
public:
	///<summary>Constructor</summary>
	DepthCamera(string _filename) :filename(_filename) {};

	///<summary>Set filename</summary>
	void setFile(string);

	///<summary>Get filename</summary>
	string getFile();

	///<summary>Read points in the file and set those to a point cloud</summary>
	PointCloud capture(int);


};

