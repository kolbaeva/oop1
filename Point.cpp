#include "Point.h"
#include <math.h>

//set x
void Point::setX(double _x ) {
	x = _x;
}

//set y
void Point::setY(double _y) {
	y = _y;
}

//set z
void Point::setZ(double _z) {
	z = _z;
}

//get x
double Point::getX() const{
	return x;
}

//get y
double Point::getY() const{
	return y;
}

//get z
double Point::getZ() const{
	return z;
}
//operator +
Point Point::operator +(const Point& p)  {
	Point tmp(x + p.x,y+p.y, z+p.z);
	return tmp;
}
//operator ==
bool Point::operator ==(const Point& p) const {
	if (x == p.x && y == p.y && z == p.z) {
		return true;
	}
	else { return false; }
	
}


//distance
double Point::distance(const Point& a) const {
	double distance;
	distance = sqrt(pow(abs(this->getX() - a.getX()), 2) + pow(abs(this->getY() - a.getY()), 2) + pow(abs(this->getZ() - a.getZ()), 2));
	return distance;
}
