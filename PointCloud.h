#pragma once
#include "Point.h"

class PointCloud
{
	
	int pointNumber;


public:
	Point* points;
	
	///<summmary>Constructor</summary>
	PointCloud(int i):pointNumber(i){ points = new Point[i]; };

	///<summary>Set</summary>
	void setPoint(int, const Point&);

	///<summary>Get point</summary>
	///<param name="n">int n is the place of the returning number</summary>
	Point getPoint(int);

	///<summary>Get point number</summary>
	int getPointnumber();

	///<summary>Assign operator<summary>
	void operator = (const PointCloud&);

	///<summary>Plus Operator</summary>
	PointCloud operator + (const PointCloud&);


	///<summary>Set Point Number</summary>
	void setPointnumber(int);


	///<summary>Delete a point</summary>
	void deletePoint(int);


};

