#include "DepthCamera.h"

//set filename
void DepthCamera::setFile(string filename) {
	this->filename = filename;
}

//get filename
string DepthCamera::getFile() {
	return filename;
}


//capture
PointCloud DepthCamera::capture(int NoP) {
	fstream file;
	file.open(filename);
	string line;
	PointCloud tmp(NoP);
	int i = 0;
	while (getline(file, line)) {

		stringstream ss(line);
		double x, y, z;
		ss >> x >> y >> z;
		Point* tmpp = new Point(x, y, z);
		tmp.setPoint(i, *tmpp);
		i++;
	}
	file.close();
	return tmp;
}
