#pragma once
#include "PointCloud.h"
class Transform
{
private:
	double angles[3];
	double trans[3];
	double transMatrix[4][4];

public:
	///<summary>Constructor</summary>
	Transform(double a[], double t[], double tm[4][4]);

	///<summary>Set Rotation</summary>
	void SetRotation(double a[]);

	///<summary>Set Translation</summary>
	void SetTranslation(double t[]);

	///<summary>do transform for point</summary>
	Point doTransform(Point);

	///<summary>do transform for pointcloud</summary>
	PointCloud doTransform(PointCloud);
};

